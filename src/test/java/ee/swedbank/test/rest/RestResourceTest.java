package ee.swedbank.test.rest;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ee.swedbank.test.model.ConsumptionInfo;
import ee.swedbank.test.model.FuelType;
import ee.swedbank.test.services.Driver;
import ee.swedbank.test.services.DriverService;
import ee.swedbank.test.services.FuelConsumptionService;

public class RestResourceTest {

	@Before
	public void setUp() {
		DriverService.addDriver("Test1", "Test1");
		DriverService.addDriver("Test2", "Test2");
	}

	@After
	public void tearDown() throws Exception {
		Driver driver1 = DriverService.getByFirstNameAndLastName("Test1", "Test1");
		Driver driver2 = DriverService.getByFirstNameAndLastName("Test2", "Test2");
		
		FuelConsumptionService.executeSql("DELETE FROM consumption_info WHERE driver_id = " + driver1.getId());
		FuelConsumptionService.executeSql("DELETE FROM consumption_info WHERE driver_id = " + driver2.getId());
		
		DriverService.deleteDriver("Test1", "Test1");
		DriverService.deleteDriver("Test2", "Test2");
	}

	@Test
	public void testAddConsumptionEntry() {

		Driver driver1 = DriverService.getByFirstNameAndLastName("Test1", "Test1");
		Driver driver2 = DriverService.getByFirstNameAndLastName("Test2", "Test2");
		
		List<ConsumptionInfo> initialConsumptionInfoList = FuelConsumptionService.getConsumptionInfoList();

		ConsumptionInfo[] consumptionArray = new ConsumptionInfo[1];
		ConsumptionInfo consumption = new ConsumptionInfo();
		consumption.setDate("2018-07-01");
		consumption.setDriverId(driver1.getId());
		consumption.setFuelType("D");
		consumption.setTotalPrice(100);
		consumption.setVolumeInLiters(25);
		consumptionArray[0] = consumption;

		FuelConsumptionService.addConsumptionInfo(consumptionArray);

		List<ConsumptionInfo> finalConsumptionInfoList = FuelConsumptionService.getConsumptionInfoList();

		assertTrue(initialConsumptionInfoList.size() < finalConsumptionInfoList.size());
		assertTrue(initialConsumptionInfoList.size() + 1 == finalConsumptionInfoList.size());
		assertTrue(finalConsumptionInfoList.get(finalConsumptionInfoList.size() - 1).getPricePerLiter() == 4);
		assertTrue(finalConsumptionInfoList.get(finalConsumptionInfoList.size() - 1).getDriverId() == driver1.getId());
		int lastInsertedEntryId = finalConsumptionInfoList.get(finalConsumptionInfoList.size() - 1)
				.getIdConsumptionInfo();
		assertTrue(lastInsertedEntryId > 0);

		ConsumptionInfo previouslySavedEntry = FuelConsumptionService.getConsumptionInfo(lastInsertedEntryId);
		assertTrue(previouslySavedEntry.getIdConsumptionInfo() == lastInsertedEntryId);

		previouslySavedEntry.setDriverId(driver2.getId());
		previouslySavedEntry.setFuelType("98");
		FuelConsumptionService.editEntry(previouslySavedEntry);

		List<ConsumptionInfo> editedConsumptionInfoList = FuelConsumptionService.getConsumptionInfoList();

		assertTrue(finalConsumptionInfoList.size() == editedConsumptionInfoList.size());
		int editedEntryId = finalConsumptionInfoList.get(finalConsumptionInfoList.size() - 1).getIdConsumptionInfo();

		ConsumptionInfo editedEntry = FuelConsumptionService.getConsumptionInfo(editedEntryId);
		assertTrue(editedEntry != previouslySavedEntry);
		assertTrue(editedConsumptionInfoList.get(editedConsumptionInfoList.size() - 1).getFuelType().equals("98"));
		assertTrue(editedConsumptionInfoList.get(editedConsumptionInfoList.size() - 1).getDriverId() == driver2.getId());
	}

	@Test
	public void testRetrieveFuelTypes() {
		List<FuelType> typeOfFuel = FuelConsumptionService.getFuelTypesInfo();
		boolean typeFound = false;
		for (FuelType fuelType : typeOfFuel) {
			if (fuelType.getFuelType().equals("95")) {
				typeFound = true;
			}
		}
		assertTrue(typeFound);

	}

	@Test
	public void testRetrieveDriver() {
		List<Driver> typeOfDriver = FuelConsumptionService.getDriverInfo();
		boolean driverFound = false;
//		for (Driver drivers : typeOfDriver) {
//			if (drivers.getFirstName().equals("Kadri") && drivers.getLastName().equals("Kuusk")) {
//				driverFound = true;
//			}
//		}
		for (int i = 0; i < typeOfDriver.size(); i++) {
			if (typeOfDriver.get(i).getFirstName().equals("Kadri") && typeOfDriver.get(i).getLastName().equals("Kuusk")) {
				driverFound = true;
			}
		}
		assertTrue(driverFound);
	}

}
