CREATE DATABASE  IF NOT EXISTS `fuel_consumption_database` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `fuel_consumption_database`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: fuel_consumption_database
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `consumption_info`
--

DROP TABLE IF EXISTS `consumption_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumption_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fuel_type` varchar(5) NOT NULL,
  `price_per_liter` decimal(7,2) NOT NULL,
  `volume_in_liters` decimal(7,2) NOT NULL,
  `total_price` decimal(7,2) NOT NULL,
  `date` date NOT NULL,
  `driver_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idconsumption_info_UNIQUE` (`id`),
  KEY `fuel_type_idx` (`fuel_type`),
  KEY `FK_fuel_consumption_info_driver_idx` (`driver_id`),
  CONSTRAINT `FK_fuel_consumption_info_driver` FOREIGN KEY (`driver_id`) REFERENCES `driver` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_fuel_consumption_info_fuel_type` FOREIGN KEY (`fuel_type`) REFERENCES `fuel_type` (`fuel_type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumption_info`
--

LOCK TABLES `consumption_info` WRITE;
/*!40000 ALTER TABLE `consumption_info` DISABLE KEYS */;
INSERT INTO `consumption_info` VALUES (68,'98',1.35,206.00,278.10,'2018-10-23',2),(69,'D',1.67,60.00,100.00,'2018-02-13',5),(70,'98',1.35,100.00,135.00,'2018-02-23',6),(71,'98',1.06,85.00,90.00,'2018-10-23',2),(72,'D',1.34,100.00,134.00,'2018-05-16',9),(73,'D',1.29,100.00,129.00,'2018-05-10',5),(75,'95',1.45,55.00,80.00,'2018-05-14',1),(78,'D',1.44,90.00,130.00,'2018-02-13',5),(79,'98',1.50,60.00,90.00,'2018-02-23',6),(80,'95',1.54,65.00,100.00,'2018-02-13',1),(84,'D',1.73,75.00,130.00,'2018-02-13',5),(85,'AN',1.25,40.00,50.00,'2018-05-10',4),(86,'D',1.60,25.00,40.00,'2018-07-01',10),(97,'98',1.64,25.00,41.00,'2018-07-01',9),(132,'95',1.50,50.00,75.00,'2018-05-18',1);
/*!40000 ALTER TABLE `consumption_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `driver`
--

DROP TABLE IF EXISTS `driver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `driver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  UNIQUE KEY `id_driver_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `driver`
--

LOCK TABLES `driver` WRITE;
/*!40000 ALTER TABLE `driver` DISABLE KEYS */;
INSERT INTO `driver` VALUES (1,'Peeter','Jalakas'),(2,'Taavi','Kadakas'),(3,'Maris','Männik'),(4,'Liina','Kask'),(5,'Kadri','Kuusk'),(6,'Kaspar','Pijlakas'),(7,'Peeter','Pihlakas'),(8,'Rauno','Märks'),(9,'Alari','Tamm'),(10,'Tarmo','Kuusk'),(11,'Leida','Labidas');
/*!40000 ALTER TABLE `driver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fuel_type`
--

DROP TABLE IF EXISTS `fuel_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fuel_type` (
  `fuel_type` varchar(5) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`fuel_type`),
  UNIQUE KEY `idfuel_type_UNIQUE` (`fuel_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fuel_type`
--

LOCK TABLES `fuel_type` WRITE;
/*!40000 ALTER TABLE `fuel_type` DISABLE KEYS */;
INSERT INTO `fuel_type` VALUES ('95','Gas 95'),('98','Gas 98'),('AN','Airplane fuel'),('D','Diesel'),('Elect','Electric power'),('IL','Tractor fuel'),('TU','Copter fuel');
/*!40000 ALTER TABLE `fuel_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(100) NOT NULL,
  `role` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'katiMati','admin','kati@mati.ee','Kati','Karu'),(2,'PilleMille','user','pille@mille.ee','Pille','Kalle'),(3,'JüriMari','user','juri@mari.ee','Jüri','Majakas');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-21 12:31:41
