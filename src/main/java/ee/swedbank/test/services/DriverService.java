package ee.swedbank.test.services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import ee.swedbank.test.services.FuelConsumptionService;

public class DriverService {

	public static List<String> getNames(String startsWith) {

		List<String> namesToReturn = new ArrayList<>();

		String sql = String.format("SELECT * FROM consumption_info WHERE name LIKE '%s%%'", startsWith);
		ResultSet nameSet = FuelConsumptionService.executeSql(sql);

		if (nameSet != null) {
			try {
				while (nameSet.next()) {
					namesToReturn.add(nameSet.getString("name"));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return namesToReturn;
	}
	
	public static Driver getByFirstNameAndLastName(String firstName, String lastName) {
		String sql = String.format("SELECT * FROM driver WHERE first_name = '%s' and last_name = '%s'", firstName, lastName);
		ResultSet resultSet = FuelConsumptionService.executeSql(sql);
		if (resultSet != null) {
			try {
				if (resultSet.next()) {
					Driver driver = new Driver();
					driver.setId(resultSet.getInt("id"));
					driver.setFirstName(resultSet.getString("first_name"));
					driver.setLastName(resultSet.getString("last_name"));
					return driver;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static void addDriver(String firstName, String lastName) {
		String sql = String.format("INSERT INTO driver (first_name, last_name) VALUES ('%s', '%s')", firstName, lastName);
		FuelConsumptionService.executeSql(sql);
	}
	
	public static void deleteDriver(String firstName, String lastName) {
		String sql = String.format("DELETE FROM driver WHERE first_name = '%s' AND last_name = '%s'", firstName, lastName);
		FuelConsumptionService.executeSql(sql);
	}
}
