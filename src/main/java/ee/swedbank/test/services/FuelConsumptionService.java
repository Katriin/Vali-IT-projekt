package ee.swedbank.test.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.swedbank.test.model.ConsumptionInfo;
import ee.swedbank.test.model.FuelType;


public class FuelConsumptionService {

	public static final String SQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/fuel_consumption_database";
	public static final String SQL_USERNAME = "root";
	public static final String SQL_PASSWORD = "tere";

	public static ResultSet executeSql(String sql) {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try (Connection conn = DriverManager.getConnection(SQL_CONNECTION_URL, SQL_USERNAME, SQL_PASSWORD)) {
				try (Statement stmt = conn.createStatement()) {
					return stmt.executeQuery(sql);
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	// @Path("/add_consumption_entry")
	public static void addConsumptionInfo(ConsumptionInfo[] consumptionInfoElements) {
		for (ConsumptionInfo consumptionInfo : consumptionInfoElements) {

			consumptionInfo.setPricePerLiter(consumptionInfo.getTotalPrice() / consumptionInfo.getVolumeInLiters());
//			consumptionInfo.setTotalPrice(consumptionInfo.getPricePerLiter() * consumptionInfo.getVolumeInLiters());

			String sql = "INSERT INTO consumption_info (fuel_type, price_per_liter, volume_in_liters, total_price, date, "
					+ "driver_id) VALUES ('%s', %s, '%s', '%s','%s', '%s')";
			sql = String.format(sql, consumptionInfo.getFuelType(), consumptionInfo.getPricePerLiter(),
					consumptionInfo.getVolumeInLiters(), consumptionInfo.getTotalPrice(), consumptionInfo.getDate(),
					consumptionInfo.getDriverId());
			executeSql(sql);
		}
	}

	// @Path("/get_consumption_entry_info_by_id")
	public static ConsumptionInfo getConsumptionInfo(int idConsumptionInfo) {
		try {
			String sql = "select * from consumption_info where id=" + idConsumptionInfo;
			ResultSet result = executeSql(sql);
			if (result != null) {
				if (result.next()) {
					ConsumptionInfo consumption = new ConsumptionInfo();
					consumption.setIdConsumptionInfo(result.getInt("id"));
					consumption.setFuelType(result.getString("fuel_type"));
					consumption.setPricePerLiter(result.getDouble("price_per_liter"));
					consumption.setVolumeInLiters(result.getDouble("volume_in_liters"));
					consumption.setTotalPrice(result.getDouble("total_price"));
					consumption.setDate(result.getString("date"));
					consumption.setDriverId(result.getInt("driver_id"));

					return consumption;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	// @Path("/get_all_consumption_entries")
	public static List<ConsumptionInfo> getConsumptionInfoList() {
		List<ConsumptionInfo> consumptionList = new ArrayList<ConsumptionInfo>();

		double totalPrice = 0;
		double volumeInLiters = 0;

		try {
			ResultSet result = executeSql("select * from consumption_info");
			if (result != null) {
				while (result.next()) {
					ConsumptionInfo consumption = new ConsumptionInfo();
					consumption.setIdConsumptionInfo(result.getInt("id"));
					consumption.setFuelType(result.getString("fuel_type"));
					consumption.setPricePerLiter(result.getDouble("price_per_liter"));
					consumption.setVolumeInLiters(result.getDouble("volume_in_liters"));
					consumption.setTotalPrice(result.getDouble("total_price"));
					consumption.setDate(result.getString("date"));
					consumption.setDriverId(result.getInt("driver_id"));

					totalPrice = Math.round(consumption.getTotalPrice() * 100.0) / 100.0;
					volumeInLiters = Math.round(consumption.getVolumeInLiters() * 100.00) / 100.0;

					consumption.setVolumeInLiters(volumeInLiters);
					consumption.setTotalPrice(totalPrice);

					consumptionList.add(consumption);

				}

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return consumptionList;
	}

	public static List<FuelType> getFuelTypesInfo() {
		List<FuelType> fuelTypeSQLList = new ArrayList<FuelType>();
		try {
			ResultSet result = null;
			result = executeSql("select * from fuel_type");
			if (result != null) {
				while (result.next()) {
					FuelType fuelTypeFromSQL = new FuelType();
					fuelTypeFromSQL.setFuelType(result.getString("fuel_type"));
					fuelTypeFromSQL.setDescription(result.getString("description"));
					fuelTypeSQLList.add(fuelTypeFromSQL);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return fuelTypeSQLList;

	}
		
		public static List<Driver> getDriverInfo() {
			List<Driver> driverInfoList = new ArrayList<>();

				ResultSet result = null;
				result = executeSql("select * from driver");
				if (result != null) { 
					try {
						while(result.next()) {
							Driver driverInfoSQL = new Driver();
							driverInfoSQL.setId(result.getInt("id"));
							driverInfoSQL.setFirstName(result.getString("first_name"));
							driverInfoSQL.setLastName(result.getString("last_name"));
							driverInfoList.add(driverInfoSQL);

						}
					} catch (SQLException e) {
					
						e.printStackTrace();
					}
				}
			
			return driverInfoList;
			
			
		}

		public static void deleteEntry(int idConsumptionInfo) {
			String sql = "DELETE FROM consumption_info where id= " + idConsumptionInfo;
			executeSql(sql);
			
		}
		public static void editEntry(ConsumptionInfo consumption) {
			consumption.setPricePerLiter(consumption.getTotalPrice() / consumption.getVolumeInLiters());
			String sql = String.format(
					"UPDATE consumption_info SET fuel_type = '%s', price_per_liter = %s, " + 
							"volume_in_liters = '%s', total_price='%s', date='%s', driver_id='%s' WHERE id = %s",
					consumption.getFuelType(), consumption.getPricePerLiter(), consumption.getVolumeInLiters(), consumption.getTotalPrice(),
					consumption.getDate(), consumption.getDriverId(), consumption.getIdConsumptionInfo());
			executeSql(sql);
		}
	
	}	
		
