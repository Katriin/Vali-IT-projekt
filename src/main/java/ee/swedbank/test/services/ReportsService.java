package ee.swedbank.test.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ee.swedbank.test.model.ComplexReport;
import ee.swedbank.test.model.ConsumptionInfo;

public class ReportsService {

	public static final String SQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/fuel_consumption_database";
	public static final String SQL_USERNAME = "root";
	public static final String SQL_PASSWORD = "tere";

	public static ResultSet executeSql(String sql) {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try (Connection conn = DriverManager.getConnection(SQL_CONNECTION_URL, SQL_USERNAME, SQL_PASSWORD)) {
				try (Statement stmt = conn.createStatement()) {
					return stmt.executeQuery(sql);
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	// vahemik: nt 2017/01 - 2017-08 --> kõik kuluraportid kuni 1. augustin 2017
// sissekanded teatud ajavahemikus

	public static List<ConsumptionInfo> getConsumptionInfoReport(int fromYear, int fromMonth, int toYear, int toMonth, int driverId) {
		String fromDate = fromYear + "-" + fromMonth + "-01";
		String toDate = toYear + "-" + toMonth + "-01";
		List<ConsumptionInfo> consumptionList1 = new ArrayList<ConsumptionInfo>();
		try {
			ResultSet result = null;
			if (driverId < 1) { 
				result = executeSql(String.format("select * from consumption_info where date >= '%s' and date < '%s'", fromDate, toDate));
			} else {
				result = executeSql(String.format("select * from consumption_info where date >= '%s' and date < '%s' and driver_id = %s", fromDate, toDate, driverId));
			}
			if (result != null) {
				while (result.next()) { 
					ConsumptionInfo consumption = new ConsumptionInfo();
					consumption.setIdConsumptionInfo(result.getInt("id"));
					consumption.setFuelType(result.getString("fuel_type"));
					consumption.setPricePerLiter(result.getDouble("price_per_liter"));
					consumption.setVolumeInLiters(result.getDouble("volume_in_liters"));
					consumption.setDate(result.getString("date"));
					consumption.setDriverId(result.getInt("driver_id"));
					consumptionList1.add(consumption);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return consumptionList1;
	}
// sorteeritud nimekiri kuude kaupa, eraldi sissekanded (kokku liitmata)
	
	public static Map<String, List<ConsumptionInfo>> sortConsumptionByMonths(List<ConsumptionInfo> consumptionList1) {
		Map<String, List<ConsumptionInfo>> result = new TreeMap<String, List<ConsumptionInfo>>();

		for (ConsumptionInfo ci : consumptionList1) {
			String yearMonth = ci.getDate().substring(0, 7); // 2018-03-23 02:56:59
			if (result.get(yearMonth) != null) {
				result.get(yearMonth).add(ci);
			} else {
				result.put(yearMonth, new ArrayList<>());
				result.get(yearMonth).add(ci);

			}
		}

		return result;
	}
//  nimekiri kuude kaupa, kogu kuu summa kokku
//	@Path("/get_total_sums_by_month")
	
	public static List<ComplexReport> getReportsByMonths(Map<String, List<ConsumptionInfo>> consumptionsByMonth, String fuelType) {
		List<ComplexReport> reportList = new ArrayList<>();

		for (String yearMonth : consumptionsByMonth.keySet()) {

			List<ConsumptionInfo> monthConsumptions = consumptionsByMonth.get(yearMonth);

			double totalPrice = 0;
			double totalVolume = 0;
			double averagePrice = 0;		
			
			for (ConsumptionInfo consumption : monthConsumptions) {
				if (fuelType == null || fuelType.equals(consumption.getFuelType())) {
					totalPrice = totalPrice + consumption.getTotalPrice();
					totalVolume = totalVolume + consumption.getVolumeInLiters();
				}
			}
			
			averagePrice = totalPrice / totalVolume;
			totalPrice = Math.round(totalPrice * 100.0) / 100.0;
			totalVolume = Math.round(totalVolume * 100.00) / 100.0;
			averagePrice = Math.round(averagePrice * 100.00) / 100.0;
			
			String[] yearMonthParts = yearMonth.split("-");
			
			ComplexReport monthReport = new ComplexReport();
			monthReport.setYear(Integer.parseInt(yearMonthParts[0]));
			monthReport.setMonth(Integer.parseInt(yearMonthParts[1]));
			monthReport.setTotalPrice(totalPrice);
			monthReport.setAveragePrice(averagePrice);
			monthReport.setTotalVolume(totalVolume);
			monthReport.setFuelType(fuelType);
			reportList.add(monthReport);

		}

		return reportList;
	}
	public static List<String> getFuelTypes() {
		List<String> fuelTypeList  = new ArrayList<>();
		try {
			ResultSet result = executeSql(String.format("select * from fuel_type"));
			if (result != null) {
				while (result.next()) {					
					fuelTypeList.add(result.getString("fuel_type"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return fuelTypeList;
	}
}
