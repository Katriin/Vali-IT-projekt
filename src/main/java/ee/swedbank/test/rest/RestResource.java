package ee.swedbank.test.rest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;


import ee.swedbank.test.model.ComplexReport;
import ee.swedbank.test.model.ConsumptionInfo;
import ee.swedbank.test.model.FuelType;

import ee.swedbank.test.model.SimpleReport;
import ee.swedbank.test.services.Driver;
import ee.swedbank.test.services.FuelConsumptionService;
import ee.swedbank.test.services.ReportsService;

@Path("/")
public class RestResource {


	@GET
	@Path("/message/db")
	@Produces(MediaType.TEXT_PLAIN)
	public String getTextFromDb() throws SQLException, ClassNotFoundException {
		// Class.forName("com.mysql.cj.jdbc.Driver");
		Class.forName("org.mariadb.jdbc.Driver");
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/valiit", "root", "tere")) {
			try (Statement stmt = conn.createStatement()) {
				try (ResultSet rs = stmt.executeQuery("SELECT simple_column FROM simpledemo")) {
					rs.first();
					String result = rs.getString(1);
					conn.close();
					return result;
				}
			}
		}
	}

	@GET
	@Path("/get_all_consumption_entries")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ConsumptionInfo> getConsumptionInfoList(@Context HttpServletRequest req) {
		if (isUserAuthorized(req, "admin") || isUserAuthorized(req, "user")) {
			return FuelConsumptionService.getConsumptionInfoList();
		} else {
			return new ArrayList<>();
		}
	}

	@GET
	@Path("/get_consumption_entry_info_by_id")
	@Produces(MediaType.APPLICATION_JSON)
	public ConsumptionInfo getConsumptionInfo(@QueryParam("consumption_info_id") int idConsumptionInfo) {
		return FuelConsumptionService.getConsumptionInfo(idConsumptionInfo);
	}

	@POST
	@Path("/add_consumption_entry")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public List<ConsumptionInfo> addConsumptionInfo(ConsumptionInfo[] consumptionInfoElements, @Context HttpServletRequest req) {
		if (isUserAuthorized(req, "admin") || isUserAuthorized(req, "user")) {
			return FuelConsumptionService.getConsumptionInfoList();
		} else {
			return new ArrayList<>();
		}
	}

	@GET
	@Path("/get_entries_by_selected_period") // nt 03.2016-06.2018 (06.ei arvata sisse) või kuu kaupa
												// 05.2018-06.2018(06. ei arvata sisse)
	@Produces(MediaType.APPLICATION_JSON)
	public List<ConsumptionInfo> getReportsPerMonth(@QueryParam("from_year") int fromYear,
			@QueryParam("from_month") int fromMonth, @QueryParam("to_year") int toYear,
			@QueryParam("to_month") int toMonth, @DefaultValue("0") @QueryParam("driver_id") int driverId) {
		return ReportsService.getConsumptionInfoReport(fromYear, fromMonth, toYear, toMonth, driverId);

	}

	@GET
	@Path("/get_simple_reports")
	@Produces(MediaType.APPLICATION_JSON)
	public List<SimpleReport> getSimpleConsumptionReports(@QueryParam("from_year") int fromYear,
			@QueryParam("from_month") int fromMonth, @QueryParam("to_year") int toYear,
			@QueryParam("to_month") int toMonth, @DefaultValue("0") @QueryParam("driver_id") int driverId) {

		List<ConsumptionInfo> consumptionInfoRecords = ReportsService.getConsumptionInfoReport(fromYear, fromMonth,
				toYear, toMonth, 0);
		Map<String, List<ConsumptionInfo>> consumptionInfoMonthlyMap = ReportsService
				.sortConsumptionByMonths(consumptionInfoRecords);
		List<ComplexReport> complexReports = ReportsService.getReportsByMonths(consumptionInfoMonthlyMap, null);

		List<SimpleReport> simpleReports = new ArrayList<>();
		for (ComplexReport complexReport : complexReports) {
			SimpleReport simpleReport = new SimpleReport();
			simpleReport.setYear(complexReport.getYear());
			simpleReport.setMonth(complexReport.getMonth());
			simpleReport.setTotalPrice(complexReport.getTotalPrice());
			simpleReports.add(simpleReport);
		}

		return simpleReports;
	}

	@GET
	@Path("/get_complex_reports")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ComplexReport> getComplexConsumptionReports(@QueryParam("from_year") int fromYear,
			@QueryParam("from_month") int fromMonth, @QueryParam("to_year") int toYear,
			@QueryParam("to_month") int toMonth, @DefaultValue("0") @QueryParam("driver_id") int driverId) {
		List<ConsumptionInfo> consumptionInfoRecords = ReportsService.getConsumptionInfoReport(fromYear, fromMonth,
				toYear, toMonth, driverId);
		Map<String, List<ConsumptionInfo>> consumptionInfoMonthlyMap = ReportsService
				.sortConsumptionByMonths(consumptionInfoRecords);

		List<String> fuelTypes = ReportsService.getFuelTypes();
		List<ComplexReport> complexReports = new ArrayList<>();
		for (String fuelType : fuelTypes) {
			complexReports.addAll(ReportsService.getReportsByMonths(consumptionInfoMonthlyMap, fuelType));
		}

		return complexReports;
	}

	@GET
	@Path("/get_fuel_types")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FuelType> getFuelTypesInfo() {
		return FuelConsumptionService.getFuelTypesInfo();
	}

	@GET
	@Path("/get_driver_info")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Driver> getDriverInfo() {
		return FuelConsumptionService.getDriverInfo();
	}
	
	@POST
	@Path("/delete_entry")
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteEntry(@FormParam("consumption_info_id") int IdConsumptionInfo) {
		FuelConsumptionService.deleteEntry(IdConsumptionInfo);
		return "OK";

	}
	
	@POST
	@Path("/edit_entry")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN) 
	public String editEntry(ConsumptionInfo consumption)  {
		FuelConsumptionService.editEntry(consumption);
		return "OK" ;
	
	}
	@POST
	@Path("/authenticate_user")
	@Produces(MediaType.TEXT_PLAIN)
	public String authenticateUser(@Context HttpServletRequest req, @FormParam("email") String email,
			@FormParam("password") String password) {
		User user = AuthenticationService.getUser(email, password);
		if (user == null) {
			// Autentimine ebaõnnestus.
			return "FAIL";
		} else {
			// Autentimine õnnestus.
			HttpSession session = req.getSession(true);
			session.setAttribute("AUTH_USER", user);
			return "SUCCESS";
		}
	}

	@GET
	@Path("/get_authenticated_user")
	@Produces(MediaType.APPLICATION_JSON)
	public User getAuthenticatedUser(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("AUTH_USER") != null) {
			// Kasutaja on sisse loginud.
			return (User) session.getAttribute("AUTH_USER");
		} else {
			// Kasutaja ei ole sisse loginud.
			return new User(); // Kasutame tühja objekti.
		}
	}

	@GET
	@Path("/logout")
	@Produces(MediaType.TEXT_PLAIN)
	public String logout(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		session.removeAttribute("AUTH_USER");
		return "SUCCESS";
	}

	private boolean isUserAuthorized(@Context HttpServletRequest req, String expectedRole) {
		HttpSession session = req.getSession(true);
		User user = null;
		if (session.getAttribute("AUTH_USER") != null) {
			// Kasutaja on sisse loginud.
			user = (User) session.getAttribute("AUTH_USER");
			return user.getRole().equals(expectedRole);
		}
		return false;
	}

	
}
