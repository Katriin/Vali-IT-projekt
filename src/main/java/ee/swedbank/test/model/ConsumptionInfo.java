package ee.swedbank.test.model;

public class ConsumptionInfo {

	private int idConsumptionInfo;
	private String fuelType;
	private double pricePerLiter;
	private double volumeInLiters;
	private String date;
	private int driverId;
	private double totalPrice;
	
	public int getIdConsumptionInfo() {
		return idConsumptionInfo;
	}

	public void setIdConsumptionInfo(int idConsumptionInfo) {
		this.idConsumptionInfo = idConsumptionInfo;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public double getPricePerLiter() {
		return pricePerLiter;
	}

	public void setPricePerLiter(double pricePerLiter) {
		this.pricePerLiter = pricePerLiter;
	}

	public double getVolumeInLiters() {
		return volumeInLiters;
	}

	public void setVolumeInLiters(double volumeInLiters) {
		this.volumeInLiters = volumeInLiters;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getDriverId() {
		return driverId;
	}

	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
}
