function logInUser() {
	var email = $('#email').val();
	var password = $('#password').val();
	
	$.ajax(
		{
			url: '/fuelapp/rest/authenticate_user',
			method: 'POST',
			data: {
				'email': email,
				'password': password
			},
			complete: function (result) {
				// Logimisprotseduur lõppenud.
				if (result.responseText == 'SUCCESS') {
					// Logimine õnnestus.
					document.location = 'fuel.html';
				} else {
					// Logimine ebaõnnestus.
					$('#errorBox').show();
				}
			}
		}
	);
}

function logOutUser() {
	$.ajax(
		{
			url: '/fuelapp/rest/logout',
			method: 'GET',
			complete: function (result) {
				$('#loginBox').show();
				$('#logoutBox').hide();
				$('#errorBox').hide();
			}
		}
	);
}

function getAuthenticatedUser() {
	$.ajax(
		{
			url: '/fuelapp/rest/get_authenticated_user',
			method: 'GET',
			complete: function (result) {
				var user = result.responseJSON;
				if (user != null && user.id > 0) {
					// Kasutaja on sisse loginud. Kuvame logout nupu.
					$('#logoutBox').show();
					$('#addFuelButton').show();
					$('#loginBox').hide()
					$('#fuelLoginButton').hide();
					$('#logOutButton').show();
				} else {
					// Kasutaja ei ole sisse loginud. Kuvame login-kasti.
					$('#loginBox').show();
					$('#addFuelButton').hide();
					$('#fuelLoginButton').show();
					$('#logOutButton').hide();
				}
			}
		}
	);
	
	
}